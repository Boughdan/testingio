//
//  SecondViewController.m
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-09.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)findUser{
    NSArray *directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [directories firstObject];
    NSString *filePathDictionary = [documents stringByAppendingPathComponent:@"user.txt"];
    NSDictionary *loadedDictionary = [NSDictionary dictionaryWithContentsOfFile:filePathDictionary];
    
    NSLog(@"%@",loadedDictionary);
    
    //[_firstName setText:[loadedDictionary objectForKey:@"firstName"]];
    //[_lastName setText:[loadedDictionary objectForKey:@"lastName"]];
    //[_userName setText:[loadedDictionary objectForKey:@"userName"]];
    //[_password setText:[loadedDictionary objectForKey:@"password"]];
}

@end
