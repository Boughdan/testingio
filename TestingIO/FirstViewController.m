//
//  FirstViewController.m
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-09.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addUser{
    /*
    NSDictionary *dict = @{@"firstName" : [_firstName text],
                           @"lastName"  : [_lastName text],
                           @"userName"  : [_userName text],
                           @"password"  : [_password text]};
    */
    
    User *user = [[User alloc] init];
    [user setFirstName:[_firstName text]];
    [user setLastName:[_lastName text]];
    [user setUserName:[_userName text]];
    [user setPassword:[_password text]];
    

    [self writeDictionary:user toFileLocation:[self retrieveDocumentPath]];
    //[self writeDictionary:dict toFileLocation:[self retrieveDocumentPath]];
    //[self writeString:dict toFileLocation:[self retrieveDocumentPath]];
}



-(void)writeDictionary : (User *) user toFileLocation : (NSString *) documents{
    //create a file manager to determine if file is already present
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePathDictionary = [documents stringByAppendingPathComponent:@"user.txt"];
    
    NSMutableDictionary *newContent = [[NSMutableDictionary alloc] init];
    NSDictionary *setuser = @{@"firstName" : [user firstName],
                              @"lastName"  : [user lastName],
                              @"userName"  : [user userName],
                              @"password"  : [user password]};
    
    if([fileManager fileExistsAtPath:filePathDictionary]){
        NSMutableDictionary *oldContent = [NSMutableDictionary dictionaryWithContentsOfFile:filePathDictionary];
        _userCounter += 1;
        NSString *userCounter = [NSString stringWithFormat:@"%d",_userCounter];
        newContent = oldContent;
        [newContent setObject:setuser forKey:userCounter];
        [newContent writeToFile:filePathDictionary atomically:YES];
    }
    else{
        _userCounter = 1;
        NSString *userCounter = [NSString stringWithFormat:@"%d",_userCounter];
        //[newContent setObject:[user forKey:userCounter];
        [newContent writeToFile:filePathDictionary atomically:YES];
        [newContent setObject:setuser forKey:userCounter];
        [newContent writeToFile:filePathDictionary atomically:YES];
        NSLog(@"%@",documents);
    }
    
}

/*
-(void)writeDictionary : (NSDictionary *) dict toFileLocation : (NSString *) documents{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePathDictionary = [documents stringByAppendingPathComponent:@"user.txt"];
 
    NSMutableDictionary *newContent = [[NSMutableDictionary alloc] init];
    
    if([fileManager fileExistsAtPath:filePathDictionary]){
        NSDictionary *oldContent = [NSDictionary dictionaryWithContentsOfFile:filePathDictionary];
        newContent = [oldContent mutableCopy];
        [newContent setObject:dict forKey:@"new"];
        [newContent writeToFile:filePathDictionary atomically:YES];
    }
    else{
        [dict writeToFile:filePathDictionary atomically:YES];
    }
    NSLog(@"%@",documents);
}
*/

/*
 -(void)writeString : (NSDictionary *) dict toFileLocation : (NSString *) documents{
 NSString *filePathDictionary = [documents stringByAppendingPathComponent:@"user.txt"];
 NSMutableString *firstName = [[dict objectForKey:@"firstName"] mutableCopy];
 NSMutableString *lastName = [[dict objectForKey:@"lastName"] mutableCopy];
 NSMutableString *userName = [[dict objectForKey:@"userName"] mutableCopy];
 NSMutableString *password = [[dict objectForKey:@"password"] mutableCopy];
 //[firstName writeToFile:filePathDictionary atomically:YES];
 //[lastName writeToFile:filePathDictionary atomically:YES];
 //[userName writeToFile:filePathDictionary atomically:YES];
 //[password writeToFile:filePathDictionary];
 NSString *test = @"Hello world!";
 
 }
 */

-(NSString *)retrieveDocumentPath{
    NSArray *directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [directories firstObject];
    return documents;
}

-(IBAction)clearTextFields{
    [_firstName setText:@""];
    [_lastName setText:@""];
    [_userName setText:@""];
    [_password setText:@""];
}

@end
