//
//  SecondViewController.h
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-09.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property(strong,nonatomic) IBOutlet UITextField *firstName;
@property(strong,nonatomic) IBOutlet UITextField *lastName;
@property(strong,nonatomic) IBOutlet UITextField *userName;
@property(strong,nonatomic) IBOutlet UITextField *password;


@end

