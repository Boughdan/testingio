//
//  User.h
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-11.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic) NSString *firstName;
@property(nonatomic) NSString *lastName;
@property(nonatomic) NSString *userName;
@property(nonatomic) NSString *password;

@end