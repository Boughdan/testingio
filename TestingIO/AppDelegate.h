//
//  AppDelegate.h
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-09.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

