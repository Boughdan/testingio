//
//  FirstViewController.h
//  TestingIO
//
//  Created by Boughdan Gibbons on 2014-12-09.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface FirstViewController : UIViewController
@property(strong,nonatomic) IBOutlet UITextField *firstName;
@property(strong,nonatomic) IBOutlet UITextField *lastName;
@property(strong,nonatomic) IBOutlet UITextField *userName;
@property(strong,nonatomic) IBOutlet UITextField *password;
@property(atomic) int userCounter;



@end

